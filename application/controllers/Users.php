<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	
	public function __construct()
	{
        parent :: __construct();
        $this->load->library('form_validation');
        $this->load->model('Users_model');

    }

public function authenticate()
	{
		// get username and password
		$pass = $this->input->post('pass');
		$user = $this->input->post('usuario');

		// check the database with that information
		$authUser = $this->users_model->authenticate($usuario, $pass);
		// return error or redirect to landing page
		if ($authUser) {
			$this->session->set_userdata('usuarios', $authUser);
			redirect(site_url(['usuarios','dashboard']));
		} else {
			$this->session->set_flashdata('error', 'Invalid username or password');
			redirect(site_url(['user','login']));
		}
	}
    public function register(){
        $this->load->view('register_view');
    }
    public function register_very(){
        if($this->input->post('summit_reg')){
            $this->form_validation->set_rules('nombre','nombre','require|trim');
            $this->form_validation->set_rules('primerApellido','primerApellido','require|trim');
            $this->form_validation->set_rules('srimerApellido','srimerApellido','require|trim');
            $this->form_validation->set_rules('telefono','telefono','require|trim');
            $this->form_validation->set_rules('direccion','direccion','require|trim');
            $this->form_validation->set_rules('usuario','usuario','require|trim');
            $this->form_validation->set_rules('pass','pass','require|trim');
            $this->form_validation->set_message('required', 'El campo  %s es obligatorio');
            if ($this-> form_validation->run() !=FALSE)
            {

            } else{
                $this->load->view('register_view');
            }           
        }
    }
    
}
