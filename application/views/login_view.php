<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Usuario</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('/application/assets/css/login.css');?>">
</head>
<body>
    <div class="container">
    <div class ="login">
        <div class="row d-flex justify-content-center mx-auto">
            <div class="col-md-6 col-xs-12 div-style">
            <form   action="<?php echo site_url(['User','authenticate']) ?>"  method="POST">
                <div class="d-flex justify-content-center mx-auto main-label" >
                    <h1>Login</h1>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control text-box" name="usuario" aria-describedby="" placeholder="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control text-box" name="pass" placeholder="">
                </div>
                <div class="form-group justify-content-center d-flex">
                    <button type="submit"   Value="Login" class="btn btn-primary button-submit">Login</button>
                </div>
                <a class="btn btn-default" href="<?php echo base_url('/application/views/register_view.php');?>">Registrar cuenta</a>
            </form>
           </div>
        </div>
    </div>
</div>
</form>
</body>
</html>