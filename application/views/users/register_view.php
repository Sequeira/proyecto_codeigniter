<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrar cuenta</title>
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
</head>
<body>


<form  action="<?= base_url().'/application/views/register_view' ?>" method="POST">
    <div class="well">
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                <input type="text" name="nombre"  class="form-control" placeholder="Nombre">
                </div>
            </div>
             <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="direccion">Dirección:</label>
                    <input type="text" name="direccion" class="form-control" placeholder="Dirección">
                </div>
            </div>
             <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="apellido1">Primer apellido:</label>
                    <input type="text" name="primerApellido" class="form-control" placeholder="Primer apellido">
                </div>
            </div>
             <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="telefono">Teléfono:</label>
                    <input type="text" name="telefono"  class="form-control" placeholder="Teléfono">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="apellido2">Segundo apellido:</label>
                    <input type="text" name="segundoApellido"  class="form-control" placeholder="Segundo apellido">
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="" id="" class="form-control" placeholder="Datos de la cuenta" tabindex="5" disabled>
        </div>
        <div class="form-group">
            <label for="">Usuario:</label>
            <input type="text" name="usuario" class="form-control" placeholder="usuario">
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" name="pass"  class="form-control" placeholder="Contraseña">
                </div>
                 <div class="form-group">
                    <label for="password2">Confrimar contraseña:</label>
                    <input type="password" name="pass_verificacion"class="form-control" placeholder="Contraseña">
                </div>

            </div>
        </div>
        <div class="row">
            <button class="btn btn-primary col-xs-6 col-md-6 nav-link btn btn-success" value="Login!" FONT SIZE=30 type="submit_reg">Registrar</button>
            <a  class="nav-link btn btn-info col-xs-6 col-md-6 " href="" >Cerrar sesion</a>
        </div>
</form><br>
    <?= validation_errors():?>
</body>
</html>